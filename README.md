# LCMM #

A clone of the LCMM R package (https://cran.r-project.org/web/packages/lcmm/index.html) with optimizations for speed and parallelism.

### How is it different? ###

 * More verbose output, print likelihood for every iteration
 * Our version of hlme (lcmm with linear link function) is now 64x faster than the original on a machine with 16 cores.

### How do I get set up? ###

Install the package from source as usual using install.packages() or use the rdevtools package.
Specify the number of threads using the environment variable OMP_NUM_THREADS, by default the maximum amount of cores will be used.

### Who do I talk to? ###

* tom.haber@uhasselt.be